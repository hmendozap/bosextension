
## Syntax
- Review in folder markers if '{' are to be used -> Yes

## Rules
"include": "#while-statement" X
"include": "#do-statement" X
"include": "#for-statement" X
"include": "#foreach-statement" X
"include": "#if-statement" 1/2 ->  Must review the Then keyword
"include": "#else-part" 1/2
"include": "#select-statement"
"include": "#return-statement" X
"include": "#break-or-continue-statement" X
"include": "#throw-statement" X
<!-- "include": "#yield-statement" -->
<!-- "include": "#await-statement" -->
"include": "#try-statement" X
"include": "#using-statement"
"include": "#local-declaration"

## Patterns

- Remember to do the rules for the statements inside the (). see line 1868 csharp.tmLanguage.json

# CSharp

(?x)\n
(?<return-type>\n
  (?<type-name>\n    
    (?:\n
      (?:ref\\s+(?:readonly\\s+)?)?   # ref return\n
      (?:\n 
        (?:(?<identifier>@?[_[:alpha:]][_[:alnum:]]*)\\s*\\:\\:\\s*)? # alias-qualification\n
        (?<name-and-type-args> # identifier + type arguments (if any)\n          \\g<identifier>\\s*\n          (?<type-args>\\s*<(?:[^<>]|\\g<type-args>)+>\\s*)?\n        )\n
        (?:\\s*\\.\\s*\\g<name-and-type-args>)* | # Are there any more names being dotted into?\n        (?<tuple>\\s*\\((?:[^\\(\\)]|\\g<tuple>)+\\))\n      )\n      (?:\\s*\\?\\s*)? # nullable suffix?\n      (?:\\s*\\[(?:\\s*,\\s*)*\\]\\s*)* # array suffix?\n
    )\n
  )\\s+\n
)\n
(?<interface-name>\\g<type-name>\\s*\\.\\s*)?\n
(\\g<identifier>)\\s*\n
(<([^<>]+)>)?\\s*\n
(?=\\()

(?x)\n
(?<!\\.)\\b(As)\\b\\s*\n 
(?<type-name>\n
  (?:\n
    (?:\n
      (?:(?<identifier>@?[_[:alpha:]][_[:alnum:]]*)\\s*\\:\\:\\s*)? # alias-qualification\n
      (?<name-and-type-args> \n\\g<identifier>\\s*\n
        (?<type-args>\\s*<(?:[^<>]|\\g<type-args>)+>\\s*)?\n
      )\n
      (?:\\s*\\.\\s*\\g<name-and-type-args>)* | # Are there any more names being dotted into?\n
      (?<tuple>\\s*\\((?:[^\\(\\)]|\\g<tuple>)+\\))\n    
    )\n
    (?:\\s*\\?\\s*)? # nullable suffix?\n
    (?:\\s*\\[(?:\\s*,\\s*)*\\]\\s*)* # array suffix?\n  
  )\n
)?",

(?x)\n \\s*\n \\b(Sub)\\s+\n (?=\n [_[:alpha:]][_[:alnum:]]*)\\s*(?=\\()

# e.g. int x OR var x
(?x) \n (?:\n \\b(Dim)\\b \n)\\s+\n(?<identifier>@?[_[:alpha:]][_[:alnum:]]*)\\b\\s*\n(?=[,)\\]])",

# Python
(?x)\n  \\s*\n 
    (?:\\b(async)\\s+)? \\b(def)\\s+\n
    (?=\n [[:alpha:]_][[:word:]]* \\s* \\( \n    )\n

(: | (?= [#'\"\\n] ))

- invocation expression regex:

(?x)\n
(?:(\\?)\\s*)? # preceding null-conditional operator?\n
(?:(\\.)\\s*)? # preceding dot?\n
(@?[_[:alpha:]][_[:alnum:]]*)\\s* # method name\n
(?<type-args>\\s*<([^<>]|\\g<type-args>)+>\\s*)?\\s* # type arguments\n
(?=\\() # open paren of argument list",

- member access expression regex:
1. match:
(?x)\n
(?:(\\?)\\s*)? # preceding null-conditional operator?\n
(\\.)\\s* # preceding dot\n
(@?[_[:alpha:]][_[:alnum:]]*)\\s* # property name\n
(?![_[:alnum:]] | \\( | (\\?)? \\[ | <)  # next character is not alpha-numeric, nor a (, [, or <. Also, test for ?[",

2. match:
(?x)\n
(\\.)?\\s*\n
(@?[_[:alpha:]][_[:alnum:]]*)\n
(?<type-params> \\s* < ([^<>] |\\g<type-params>)+ >\\s*)\n
(?=\n
  (\\s*\\?)?\n
  \\s*\\.\\s* @?[_[:alpha:]][_[:alnum:]]*\n
)",

3. match:
(?x)\n
(@?[_[:alpha:]][_[:alnum:]]*)\n
(?=\n
  (\\s*\\?)?\n
  \\s*\\.\\s*@?[_[:alpha:]][_[:alnum:]]*\n
)",

- cast expression regex:
(?x)\n
(\\()\\s*\n
(?<type-name>\n  
  (?:\n
    (?:\n
      (?:(?<identifier>@?[_[:alpha:]][_[:alnum:]]*)\\s*\\:\\:\\s*)? # alias-qualification\n
      (?<name-and-type-args> # identifier + type arguments (if any)\n \\g<identifier>\\s*\n
        (?<type-args>\\s*<(?:[^<>]|\\g<type-args>)+>\\s*)?\n
      )\n
      (?:\\s*\\.\\s*\\g<name-and-type-args>)* | # Are there any more names being dotted into?\n
      (?<tuple>\\s*\\((?:[^\\(\\)]|\\g<tuple>)+\\))\n
    )\n
    (?:\\s*\\?\\s*)? # nullable suffix?\n
    (?:\\s*\\[(?:\\s*,\\s*)*\\]\\s*)* # array suffix?\n
  )\n
)\\s*\n
(\\))
(?=\\s*@?[_[:alnum:]\\(])",

- property expression regex:
(?x)\n\n# The negative lookahead below ensures that we don't match nested types\n# or other declarations as properties.\n
(?![[:word:][:space:]]*\\b(?:class|interface|struct|enum|event)\\b)\n
(?<return-type>\n
  (?<type-name>\n    (?:\n      (?:ref\\s+(?:readonly\\s+)?)?   # ref return\n      (?:\n        (?:(?<identifier>@?[_[:alpha:]][_[:alnum:]]*)\\s*\\:\\:\\s*)? # alias-qualification\n        (?<name-and-type-args> # identifier + type arguments (if any)\n          \\g<identifier>\\s*\n          (?<type-args>\\s*<(?:[^<>]|\\g<type-args>)+>\\s*)?\n        )\n        (?:\\s*\\.\\s*\\g<name-and-type-args>)* | # Are there any more names being dotted into?\n        (?<tuple>\\s*\\((?:[^\\(\\)]|\\g<tuple>)+\\))\n      )\n      (?:\\s*\\?\\s*)? # nullable suffix?\n      (?:\\s*\\[(?:\\s*,\\s*)*\\]\\s*)* # array suffix?\n    )\n  )\\s+\n
)\n
(?<interface-name>\\g<type-name>\\s*\\.\\s*)?\n
(?<property-name>\\g<identifier>)\\s*\n
(?=\\{|=>|$)",

- local variable declaration regex:
(?x)\n
(?:\n
  (?: (\\bref)\\s+ (?:(\\breadonly)\\s+)?)? (\\bvar\\b) | # ref local\n 
  (?<type-name>\n    (?:\n      (?:ref\\s+(?:readonly\\s+)?)?   # ref local\n      (?:\n        (?:(?<identifier>@?[_[:alpha:]][_[:alnum:]]*)\\s*\\:\\:\\s*)? # alias-qualification\n        (?<name-and-type-args> # identifier + type arguments (if any)\n          \\g<identifier>\\s*\n          (?<type-args>\\s*<(?:[^<>]|\\g<type-args>)+>\\s*)?\n        )\n        (?:\\s*\\.\\s*\\g<name-and-type-args>)* | # Are there any more names being dotted into?\n        (?<tuple>\\s*\\((?:[^\\(\\)]|\\g<tuple>)+\\))\n      )\n      (?:\\s*\\?\\s*)? # nullable suffix?\n      (?:\\s*\\[(?:\\s*,\\s*)*\\]\\s*)* # array suffix?\n    )\n  
  )\n
)\\s+\n
(\\g<identifier>)\\s*\n
(?=,|;|=|\\))

(?x)\n (?:\n
  (?:\\s*\n\\b(Dim)\\b\\s+)
  (?<identifier>@?[_[:alpha:]][_[:alnum:]]*)\\s*
  (?:\\b(As)\\b\\s+)
  (?<type-name>\n (?:\n (?:\n (?<name-and-type-args>\n
    (?<identifier>@?[_[:alpha:]][_[:alnum:]]*)\\s*\n
    (?<type-args>\\s*<(?:[^<>]|\\g<type-args>)+>\\s*)?\n)\n
    (?:\\s*\\.\\s*\\g<name-and-type-args>)* \n)\n
    (?:\\s*\\?\\s*)? # nullable suffix?\n
    (?:\\s*\\[(?:\\s*,\\s*)*\\]\\s*)* # array suffix?\n)\n
  )\\s+\n
) | (?: (\\bVar\\b)\\s+\n(\\g<identifier>)\\s*\n)
(?=$)

- field expression regex:
(?x)\n
(?:\\s*\n\\b(Dim)\\b\\s+)(?<identifier>@?[_[:alpha:]][_[:alnum:]]*)\\s*
(?:\\b(As)\\b\\s+)
(?<type-name>\n (?:\n (?:\n (?<name-and-type-args>\n (?<identifier>@?[_[:alpha:]][_[:alnum:]]*)\\s*\n (?<type-args>\\s*<(?:[^<>]|\\g<type-args>)+>\\s*)?\n)\n (?:\\s*\\.\\s*\\g<name-and-type-args>)* \n)\n (?:\\s*\\?\\s*)? # nullable suffix?\n (?:\\s*\\[(?:\\s*,\\s*)*\\]\\s*)* # array suffix?\n)\n)\\s+\n
(?=$)

(?x)\n
(?<type-name>\n
  (?:\n
    (?:\n
      (?: (?<identifier>@?[_[:alpha:]][_[:alnum:]]*)\\s*\\:\\:\\s*)? # alias-qualification\n
      (?<name-and-type-args> # identifier + type arguments (if any)\n
        \\g<identifier>\\s*\n
        (?<type-args>\\s*<(?:[^<>]|\\g<type-args>)+>\\s*)?\n
      )\n
      (?:\\s*\\.\\s*\\g<name-and-type-args>)* | # Are there any more names being dotted into?\n
      (?<tuple>\\s*\\((?:[^\\(\\)]|\\g<tuple>)+\\))\n    
    )\n
    (?:\\s*\\?\\s*)? # nullable suffix?\n
    (?:\\s*\\[(?:\\s*,\\s*)*\\]\\s*)* # array suffix?\n
  )\n
)\\s+\n
(\\g<identifier>)\\s* # first field name\n
(?!=>|==)
(?=$)",

- event declaration expression regex:
(?x)\n\\b(Event)\\b\\s*\n 
(?<return-type>\n 
  (?<type-name>\n 
    (?:\n 
      (?:\n 
        (?<name-and-type-args>\n (?<identifier>@?[_[:alpha:]][_[:alnum:]]*)\\s*\n (?<type-args>\\s*<(?:[^<>]|\\g<type-args>)+>\\s*)?\n
        )\n
        (?:\\s*\\.\\s*\\g<name-and-type-args>)* \n
      )\n 
      (?:\\s*\\?\\s*)? # nullable suffix?\n 
      (?:\\s*\\[(?:\\s*,\\s*)*\\]\\s*)* # array suffix?\n
    )\n
  )\\s+\n
)\n
(?<interface-name>\\g<type-name>\\s*\\.\\s*)?\n 
(?<event-names>\\g<identifier>(?:\\s*,\\s*\\g<identifier>)*)\\s*\n
(?=$)

- foreach statement regex:
(?x)\n(?<=Foreach\\s)(?<identifier>@?[_[:alpha:]][_[:alnum:]]*)\\s+\n\\b(In)\\b

# Disabled or missing statements
named-argument
<!-- "include": "#yield-statement" -->
<!-- "include": "#await-statement" -->

# Missing Expression
<!-- preprocessor -->
comment - X
<!-- checked-unchecked-expression -->
typeof-or-default-expression - X
nameof-expression - X
throw-expression - X
<!-- interpolated-string -->
<!-- verbatim-interpolated-string -->
this-or-base-expression - X
conditional-operator - X
expression-operators - X
<!-- await-expression -->
<!-- query-expression -->
as-expression - X
<!-- is-expression -->
<!-- anonymous-method-expression -->
object-creation-expression - X
array-creation-expression - X
<!-- anonymous-object-creation-expression -->
invocation-expression - X
member-access-expression - X
element-access-expression - X
cast-expression -> Need to review the use with BOS
literal - X
parenthesized-expression - X
<!-- tuple-deconstruction-assignment -->
initializer-expression - X
identifier - X

# Class-Struct-Members
comment - X
storage-modifier - X
type-declarations - X
property-declaration - 1/2 Review property declaration and block statements
  to be used inside the get and set properties. Also review return type of property
field-declaration - X
event-declaration - 1/2 Add event accessors and blocks, if necessary
<!-- indexer-declaration - Review use in BOS -->
variable-initializer - Review use in BOS Classes
constructor-declaration - Review extra cases, because Sub declaration covers it
destructor-declaration - Same as above
<!-- operator-declaration - I think is not used in BOS -->
<!-- conversion-operator-declaration - I think is not used in BOS -->
method-declaration - X
attribute-section - X
<!-- punctuation-semicolon -->