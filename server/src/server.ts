import {
    createConnection,
    TextDocuments,
    TextDocument,
    Diagnostic,
    DiagnosticSeverity,
    ProposedFeatures,
    InitializeParams,
    DidChangeConfigurationNotification,
    CompletionItem,
    CompletionItemKind,
    TextDocumentPositionParams,
    Hover
} from 'vscode-languageserver';
import { interfaces } from 'mocha';

// Create a connection for the server. The connection uses Node's IPC as a transport.
// Also include all preview / proposed LSP features.
let connection = createConnection(ProposedFeatures.all);

//  Create a simple bos document manager. The bos document manager
// supports full document sync only
let documents: TextDocuments = new TextDocuments();

let hasConfigurationCapability: boolean = false;
let hasWorkspaceCapability: boolean = false;
let hasDiagnosticRelatedInformationCapability: boolean = false;

connection.onInitialize((params: InitializeParams) => {
    let capabilities = params.capabilities;

    //Does client support the 'workspace/configuration' requests
    hasConfigurationCapability = !!(capabilities.workspace && !!capabilities.workspace.configuration);
    hasWorkspaceCapability = !!(capabilities.workspace && !!capabilities.workspace.workspaceFolders);
    hasDiagnosticRelatedInformationCapability = 
        !!(capabilities.textDocument &&
            capabilities.textDocument.publishDiagnostics &&
            capabilities.textDocument.publishDiagnostics.relatedInformation);

    return {
        capabilities: {
            textDocumentSync: documents.syncKind,
            // Tell the client that the server supports code completion
            completionProvider:  {
                resolveProvider: true,
                triggerCharacters: ['.']
            },
            signatureHelpProvider: {
                triggerCharacters: ['(']
            }
        }
    };
});

connection.onInitialized(() => {
    if (hasConfigurationCapability) {
        // Register for all configuration changes
        connection.client.register(
            DidChangeConfigurationNotification.type,
            undefined
        );
    }

    if (hasWorkspaceCapability) {
        connection.workspace.onDidChangeWorkspaceFolders(_event => {
            connection.console.log('Evento de workspace Recibido');
        });
    }
});

// The example settings
interface ExampleSettings {
    maxNumberOfProblems: number;
}

// The global settings, used when the `workspace/configuration` request is not supported by the client.
// Please note that this is not the case when using this server with the client provided in this example
// but could happen with other clients.
const defaultSettings: ExampleSettings = { maxNumberOfProblems: 10 };
let globalSettings : ExampleSettings = defaultSettings;

// Cache the settings of all open documents
let documentSettings: Map<string, Thenable<ExampleSettings>> = new Map();

connection.onDidChangeConfiguration(change => {
    if (hasConfigurationCapability){
        documentSettings.clear();
    } else {
        globalSettings = <ExampleSettings>(
            (change.settings.languageServerBOS || defaultSettings)
        );
    }
    // Revalidate all open documents
    documents.all().forEach(validateTextDocument);
});

function getDocumentSettings(resource:string): Thenable<ExampleSettings> {
    if (!hasConfigurationCapability){
        return Promise.resolve(globalSettings);
    }
    let result = documentSettings.get(resource);
    if (!result) {
        result = connection.workspace.getConfiguration({
            scopeUri: resource,
            section: 'languageServerBOS'
        });
        documentSettings.set(resource, result);
    }
    return result;
}

// Only keep settings for open documents
documents.onDidClose(event => {
    documentSettings.delete(event.document.uri);
});

// The content of a text decument has changed. This event is emitted when the text
// document first opened tor when its content has changed
documents.onDidChangeContent(change => {
    validateTextDocument(change.document);
});

async function validateTextDocument(textDocument: TextDocument): Promise<void> {
    // In this example, we get the settings for every validate run
    let settings = await getDocumentSettings(textDocument.uri);

    // Validator creates diagnostics for all uppercases words length 2 and more
    let text = textDocument.getText();
    // For RegEx it has to be enclosed in /:regexp:/
    let pattern = /\b[A-Z]{2,}\b/g;
    let matchRegEx: RegExpExecArray | null;

    let problems = 0;
    let diagnostics: Diagnostic[] = [];
    while ((matchRegEx = pattern.exec(text)) && problems < settings.maxNumberOfProblems) {
        problems++;
        let diagnostic: Diagnostic = {
            severity: DiagnosticSeverity.Warning,
            range: {
                start: textDocument.positionAt(matchRegEx.index),
                end: textDocument.positionAt(matchRegEx.index + matchRegEx[0].length)
            },
            message: `${matchRegEx[0]} is all upper case`,
            source: 'example'
        };

        if (hasDiagnosticRelatedInformationCapability) {
            diagnostic.relatedInformation = [
                {
                    location: {
                        uri: textDocument.uri,
                        range: Object.assign({}, diagnostic.range)
                    },
                    message: 'Spelling matters'
                },
                {
                    location: {
                        uri: textDocument.uri,
                        range: Object.assign({}, diagnostic.range)
                    },
                    message: 'Particularly for names'
                }
            ];
        }
        diagnostics.push(diagnostic);
    }
    // Send the computed diagnostics to VSCode
    connection.sendDiagnostics({ uri: textDocument.uri, diagnostics });
}

connection.onDidChangeWatchedFiles(change => {
    // Monitored files have a change in VSCode
    connection.console.log('Evento de cambio de archivo recibido');
});

connection.onHover(
    (_textDocPosition: TextDocumentPositionParams): Thenable<Hover> => {
        _textDocPosition.position

        let result:Hover;
        result.contents = "asteorua";
        return result;

    }
);

// Handler provides initials list of completion items.
connection.onCompletion(
    (_textDocumentPosition: TextDocumentPositionParams): CompletionItem[] => {
        // The pass parameter contains the position of the text document in
        // which code complete got requested. For the example we ignore this
        // info and always provide the same completion items.
        const simpleCompletion = CompletionItem.create('TypeScript');
        simpleCompletion.kind = CompletionItemKind.Text;
        simpleCompletion.data = 1

        const simpleCompletion2 = CompletionItem.create('Javascript')
        simpleCompletion2.kind = CompletionItemKind.Text;
        simpleCompletion2.data = 2

        let allItems: CompletionItem[]  = [
            simpleCompletion, simpleCompletion2
        ]
        return allItems;
    }
);

// This handler resolve additional information for the item selected in
// the completion list.
connection.onCompletionResolve(
    (item: CompletionItem): CompletionItem => {
        if (item.data === 1) {
            (item.detail = 'TS Deets'), (item.documentation = 'TS Docs')
        } else if (item.data === 2) {
            (item.detail = 'JS Deets'), (item.documentation = 'JS Docs')
        }
        return item;
    }
);

// Make the text document manager listen on the connection
// for open, change and close text document events
documents.listen(connection);

// Listen on the connection
connection.listen();