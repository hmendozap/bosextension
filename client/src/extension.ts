
import * as path from 'path';
import { workspace, ExtensionContext } from 'vscode';

import {
    LanguageClient,
    LanguageClientOptions,
    ServerOptions,
    TransportKind
} from 'vscode-languageclient';

let client: LanguageClient;

export function activate(context: ExtensionContext) {
    let serverModule = context.asAbsolutePath(
        path.join('server', 'out', 'server.js')
    );

    // Debug Options for server
    // --inspect=6009: runs the server in Node's Inspector mode so VS Code can attach to the server for debugging
    let debugOptions = {
        execArgv: ['--nolazy', '--inspect=6009']
    };

     let serverOptions: ServerOptions = {
         run: { module: serverModule, transport: TransportKind.ipc },
         debug : {
             module: serverModule,
             transport: TransportKind.ipc,
             options: debugOptions
         }
     };

     // Options to control the language client
     let clientOptions: LanguageClientOptions =  {
         // Register Server for bos documents
         documentSelector : [{ scheme : 'file', language: 'bos' }],
         synchronize: {
             // Notify to the server about the file changes to  '.clientrc' files contained in workspace
             fileEvents: workspace.createFileSystemWatcher('**/.clientrc')
         }
     };

     //Create the language client and start the client.
     client = new LanguageClient(
         'BOS-LanguageServer',
         'Bos Client Example',
         serverOptions,
         clientOptions
     );

     //Start client. will also start server
     client.start();

}

export function deactivate() : Thenable<void> | undefined {
    if (!client){
        return undefined;
    }
    return client.stop();
}