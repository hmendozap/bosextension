'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
// import * as vscode from 'vscode';
import {window, commands, Disposable, ExtensionContext, StatusBarAlignment,
    StatusBarItem, TextDocument} from 'vscode'

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: ExtensionContext) {

    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    console.log('Congratulations, your extension "bos" is now active!');

    // create a new word counter
    let wordCounter = new WordCounter();
    let controller = new WordCounterController(wordCounter);

    // Add to a list of disposables which are disposed when this extension is deactivated.
    context.subscriptions.push(controller);
    context.subscriptions.push(wordCounter);

    //// The command has been defined in the package.json file
    //// Now provide the implementation of the command with  registerCommand
    //// The commandId parameter must match the command field in package.json
    // let disposable = commands.registerCommand('extension.sayHello', () => {
        //// The code you place here will be executed every time your command is executed
        //// wordCounter.UpdateWordCount();
        //// Display a message box to the user
        // window.showInformationMessage('Hello World!');
    // });
    // context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {
}

class WordCounter {
    private _statusBarItem: StatusBarItem =
      window.createStatusBarItem(StatusBarAlignment.Left);

    public UpdateWordCount() {
        //Get current text editor
        let editor = window.activeTextEditor;
        if (!editor) {
            this._statusBarItem.hide();
            return;
        }

        let doc = editor.document;

        //Only update status if a BOS file
        if (doc.languageId == "bos") {
            let wordCount = this._getWordCount(doc);

            //Update status bar
            this._statusBarItem.text = wordCount !== 1 ? `${wordCount} Words` : '1 Word';
            this._statusBarItem.show();
        } else {
            this._statusBarItem.hide();
        }
    }

    public _getWordCount(doc: TextDocument): number {
        let docContent = doc.getText();

        //Parse unwanted whitespaces so the split is accurate
        docContent = docContent.replace("/(< ([^>])</g)", '').replace("/\s+/g", ' ');
        docContent = docContent.replace("/^\s\s*/", '').replace("/\s\s*$/", '');

        let wordCount = 0;
        if (docContent !== "") {
            wordCount = docContent.split(" ").length;
        }
        return wordCount;
    }

    dispose() {
        this._statusBarItem.dispose();
    }
}

class WordCounterController {
    private _wordCounter: WordCounter;
    private _disposable: Disposable;

    constructor(wordCounter: WordCounter) {
        this._wordCounter = wordCounter;

        // subscribe to the selection change and editor activation events
        let subscriptions: Disposable[] = []
        window.onDidChangeTextEditorSelection(this._onEvent, this, subscriptions);
        window.onDidChangeActiveTextEditor(this._onEvent, this, subscriptions);

        // update the counter for the current file
        this._wordCounter.UpdateWordCount();

        // create a combined disposable from both event subscriptions
        // ...subscriptions is a Rest Parameter, is to pass a variable number
        // of arguments to your function. In this case is used to pass the 
        // array subscriptions
        this._disposable = Disposable.from(...subscriptions);
    }

    dispose() {
        this._disposable.dispose();
    }

    private _onEvent() {
        this._wordCounter.UpdateWordCount();
    }
}