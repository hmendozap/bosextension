# Welcome to your VS Code Extension

## What's in the folder
* This folder contains all of the files necessary for your extension.
* `package.json` - this is the manifest file in which you declare your language support and define
the location of the grammar file that has been copied into your extension.
* `syntaxes/bos.tmLanguage.json` - this is the Text mate grammar file that is used for tokenization.
* `language-configuration.json` - this the language configuration, defining the tokens that are used for
comments and brackets.

## Get up and running straight away
* Make sure the language configuration settings in `language-configuration.json` are accurate.
* Press `F5` to open a new window with your extension loaded.
* Create a new file with a file name suffix matching your language.
* Verify that syntax highlighting works and that the language configuration settings are working.

## Make changes
* You can relaunch the extension from the debug toolbar after making changes to the files listed above.
* You can also reload (`Ctrl+R` or `Cmd+R` on Mac) the VS Code window with your extension to load your changes.

## Add more language features
* To add features such as intellisense, hovers and validators check out the VS Code extenders documentation at
https://code.visualstudio.com/docs

## Install your extension
* To start using your extension with Visual Studio Code copy it into the `<user home>/.vscode/extensions` folder and restart Code.
* To share your extension with the world, read on https://code.visualstudio.com/docs about publishing an extension.


## Personal Notes
* Even if BOS extends the language, you can always go to csharp.tmLanguage.json for hints
i.e. using-directive

* It looks like VSCode uses Rust RegExes

* Review class-declarations patterns in index 0

* base types in this case is extends, should be changed to keyword

* typename-patterns-0: is typename-alias (::) Operator is not included

* Review the end of structure and interface statements

* ${workspaceFolder} should always be used. (root is deprecated)
 
* When working with `yarn` instead of `npm` remember to change the configuration of package to use in settings. Also review the Typescript version that you have installed, and try to reinstall everything with yarn, also as recommendation, run the commands of `package.json` in the terminal, in order to be sure that they work

* `Thenable` in javascript, means then-able, because it's a piece of code that complies with the specification of a promise, in the sense that is capable of returning a `then` method.
````js
  getJSON().then(data => {something})
````

## TODO:

* Check type-arguments
* Doubt in enum-declaration patterns[0].patterns[2] "As Operator" - maybe change it to as-expression. 
* Remember to replace the end match of class-declarations

* type-parameter-list
* type
* punctuation-comma
* script-top-level
* delegate-declaration


(?x) -> means free spacing to ignore whitespace between tokens and allow # comments
(?:) -> matches a token; but cannot be used for extracting substring or backreference

(?x)\n
(?:(?:\\b(ref|params|out|in|this)\\b)\\s+)?\n
(?<type-name>\n  
  (?:\n    
    (?:\n
      (?:(?<identifier> @?[_[:alpha:]][_[:alnum:]]*) \\s* \\:\\: \\s*)? # alias-qualification \n      
      (?<name-and-type-args> # identifier + type arguments (if any)\n\\g<identifier>\\s*\n        
        (?<type-args>\\s*<(?:[^<>]|\\g<type-args>)+>\\s*)?\n      
      )\n      
      (?:\\s*\\.\\s*\\g<name-and-type-args>)* | # Are there any more names being dotted into?\n      
      (?<tuple>\\s*\\( (?:[^\\(\\)]|\\g<tuple>)+\\) )\n    
    )\n    
    (?:\\s*\\?\\s*)? # nullable suffix?\n    
    (?:\\s*\\[(?:\\s*,\\s*)*\\]\\s*)* # array suffix?\n  
  )\n
)\\s+\n
(\\g<identifier>) <- parameter name

